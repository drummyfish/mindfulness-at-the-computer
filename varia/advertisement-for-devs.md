Python Programmer
Category:   Software Development
Location:   Remote
Posted:       2019-04-07


Description

We're looking for Python developers for the FOSS desktop application project "Mindfulness at the Computer"

# App description
Mindfulness at the Computer is a mindfulness and self-care application for people spending many hours in front of the computer

Features:
- Receive notifications to remember to be mindful of breathing
- Follow and track your breathing (in, out)
- Breathe with text help ("Breathing in I know I am breathing in") and write your own texts
- Get reminders when it's time to take a break from the computer (and take care of your body)

More information on the application website: https://mindfulness-at-the-computer.github.io

# Who we are looking for
People familiar with Python development. (This is a free software project and unfortunately i am not able to pay for help)

The work that needs to be done are things like:
- Building and packaging for Windows, Ubuntu/Debian, MacOS, etc
- Refactoring and improving code quality
- Writing test cases with pytest or with unittest

Please note that multiple people may be accepted

# GitHub page
https://github.com/mindfulness-at-the-computer/mindfulness-at-the-computer

# License
GPLv3

# Technology
Programming language: Python
Front-end: Qt5 (PyQt5)
Back-end: SQLite

# Contact


