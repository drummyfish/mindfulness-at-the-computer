import pathlib


def write_package_modules_to_reference_file(i_package: str, i_file_path: str):
    # package = i_package.split(".")
    file_list = []
    relative_path_str = i_package.replace(".", "/")
    for file in pathlib.Path.iterdir(pathlib.Path.cwd() / relative_path_str):
        fn = file.name
        if fn == "intro_dlg.py":
            continue  # TODO: Intro dialog has indentation problems
        if fn.endswith(".py"):
            file_list.append(file)
    ref_file_path = pathlib.Path(i_file_path)
    with open(ref_file_path, "w+") as f:
        files_content = f"<!-- Auto-generated using {__file__} -->\n"
        for file in file_list:
            module_name = file.name.removesuffix(".py")
            files_content += f"::: {i_package}." + module_name + "\n"
        f.write(files_content)


write_package_modules_to_reference_file("matc", "docs/reference.md")
write_package_modules_to_reference_file("matc.gui", "docs/gui_reference.md")
